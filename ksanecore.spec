Name:          ksanecore
Summary:       Library providing logic to interface scanners
Version:       23.08.5
Release:       2

License:       BSD and LGPLv2.1-only and LGPLv3.0-only
URL:           https://invent.kde.org/libraries/ksanecore
Source0:       http://download.kde.org/stable/release-service/%{version}/src/%{name}-%{version}.tar.xz

BuildRequires: cmake  zlib-devel
BuildRequires: extra-cmake-modules
BuildRequires: gcc-c++
BuildRequires: cmake(KF5I18n)
BuildRequires: qt5-qtbase-devel 
BuildRequires: pkgconfig(sane-backends)

%description
%{summary}.

%package devel
Summary:  Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary}.


%prep
%autosetup -n %{name}-%{version}


%build
%{cmake_kf5}
%cmake_build


%install
%cmake_install
%find_lang %{name} --all-name --with-html


%files -f %{name}.lang
%doc README.md
%license LICENSES/*
%{_kf5_libdir}/libKSaneCore.so.*

%files devel
%{_includedir}/KSaneCore/*
%{_kf5_libdir}/cmake/KSaneCore/*
%{_kf5_libdir}/libKSaneCore.so


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Fri Mar 15 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Tue Jan 09 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- update verison to 23.08.4

* Thu Dec 14 2023 misaka00251 <liuxin@iscas.ac.cn> - 23.04.3-1
- Init package
